package ru.thehep;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Handler implements Listener {
	
	private PlayerC plugin;

	public Handler(PlayerC playerC) {
		plugin = playerC;
	}
	
	@EventHandler
	public void Join(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		List<String> msg = plugin.getConfig().getStringList("messages.join");
		for(String s : msg) {
			s = s.replace("&", "\u00a7");
			p.sendMessage(s);
	}
		
		for (Player p1 : Bukkit.getOnlinePlayers()) {
			File PlayerC = new File(plugin.getDataFolder() + File.separator + "PlayerC.json");
			FileConfiguration users = YamlConfiguration.loadConfiguration(PlayerC);
			List<Integer> list = users.getIntegerList("users");
			if(list == null) list = new ArrayList<Integer>();
			if(list.contains(p1.getServer().getOnlinePlayers().size())) return;
			if(p1.isOnline()) {
				list.add(p1.getServer().getOnlinePlayers().size());
			}
			users.set("users", list);
			try {
				users.save(PlayerC);
				} catch (IOException e1) {
					e1.printStackTrace();
			}
		}
	}
}








