package ru.thehep;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class PlayerC extends JavaPlugin{
	public final String pluginPrefix = "[PlayerC]";
	
	public void onEnable() {
		
		File config = new File(getDataFolder() + File.separator + "config.yml");
		if(!config.exists()) {
			getLogger().info("Creating config...");
			getConfig().options().copyDefaults(true);
			saveDefaultConfig();
		}
		
		Bukkit.getPluginManager().registerEvents(new Handler(this), this);
		File PlayerC = new File(getDataFolder() + File.separator + "PlayerC.json");
		if(!PlayerC.exists()) {
			try {
				PlayerC.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		getLogger().info("Plugin Enbled!");
	}
	
	public void onDisable() {
		getLogger().info("Plugin Disbled!");
	}

}
